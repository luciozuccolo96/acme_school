﻿using System;

namespace ACME.School.Core.Models
{
    public class Enrollment
    {
        public Student Student { get; private set; }
        public Course Course { get; private set; }
        public DateTime EnrollmentDate { get; private set; }

        public Enrollment(Student student, Course course, DateTime? enrollmentDate = null)
        {
            Student = student ?? throw new ArgumentNullException(nameof(student));
            Course = course ?? throw new ArgumentNullException(nameof(course));
            EnrollmentDate = enrollmentDate ?? DateTime.Now;
        }
    }
}
