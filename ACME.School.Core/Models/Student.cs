﻿using ACME.School.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.School.Core.Models
{
    public class Student
    {
        public string Name { get; private set; }
        public int Age { get; private set; }
        public List<Enrollment> Enrollments { get; private set; } = new List<Enrollment>();

        public Student(string name, int age)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Student name cannot be null or empty.");
            }

            if (age < 18)
            {
                throw new ArgumentException("Only adults can register.");
            }

            Name = name;
            Age = age;
        }

        public void EnrollInCourse(Course course, IPaymentGateway paymentGateway)
        {
            if (course.RegistrationFee > 0)
            {
                paymentGateway.ProcessPayment(course.RegistrationFee);
            }
            Enrollments.Add(new Enrollment(this, course));
        }
    }
}
