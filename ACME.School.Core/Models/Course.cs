﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ACME.School.Core.Models
{
    public class Course
    {
        public string Name { get; private set; }
        public decimal RegistrationFee { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }

        public Course(string name, decimal registrationFee, DateTime startDate, DateTime endDate)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentException("Course name cannot be null or empty.");
            }
            if (startDate > endDate)
            {
                throw new ArgumentException("The start date cannot be later than the end date.");
            }

            Name = name;
            RegistrationFee = registrationFee;
            StartDate = startDate;
            EndDate = endDate;
        }
    }
}
