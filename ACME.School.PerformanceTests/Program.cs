﻿using BenchmarkDotNet.Running;

namespace ACME.School.Tests
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var summary = BenchmarkRunner.Run<PerformanceTests>();
        }
    }
}