﻿using System;
using System.Collections.Generic;
using BenchmarkDotNet.Attributes;
using ACME.School.Core.Models;
using ACME.School.Core.Services;
using NSubstitute;
using System.Linq;

namespace ACME.School.Tests
{
    public class PerformanceTests
    {
        private List<Student> students;
        private List<Course> courses;
        private IPaymentGateway paymentGateway;

        [GlobalSetup]
        public void Setup()
        {
            students = new List<Student>();
            courses = new List<Course>();
            paymentGateway = Substitute.For<IPaymentGateway>();

            // Reducir el número de estudiantes y cursos para una ejecución más rápida
            for (int i = 0; i < 100; i++)
            {
                students.Add(new Student($"Student {i}", 20));
                courses.Add(new Course($"Course {i}", 100, DateTime.Now, DateTime.Now.AddMonths(1)));
            }
        }

        [Benchmark]
        public void EnrollStudentsInCourses()
        {
            foreach (var student in students)
            {
                foreach (var course in courses)
                {
                    student.EnrollInCourse(course, paymentGateway);
                }
            }
        }

        [Benchmark]
        public void ListEnrollmentsWithinDateRange()
        {
            var startDate = DateTime.Now;
            var endDate = DateTime.Now.AddMonths(1);

            foreach (var student in students)
            {
                var enrollments = student.Enrollments.Where(e => e.EnrollmentDate >= startDate && e.EnrollmentDate <= endDate).ToList();
            }
        }
    }
}
