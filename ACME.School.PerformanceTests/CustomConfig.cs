﻿using BenchmarkDotNet.Configs;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Loggers;
using BenchmarkDotNet.Exporters;
using BenchmarkDotNet.Columns;
using BenchmarkDotNet.Exporters.Csv;

namespace ACME.School.PerformanceTests
{
    public class CustomConfig : ManualConfig
    {
        public CustomConfig()
        {
            AddJob(Job.Default
                .WithWarmupCount(1)  // Reducir el número de iteraciones de calentamiento
                .WithIterationCount(3));  // Reducir el número de iteraciones de medición

            // Agregar un logger para ver el progreso en la consola
            AddLogger(ConsoleLogger.Default);

            // Agregar exportadores para persistir los resultados
            AddExporter(MarkdownExporter.Default);
            AddExporter(HtmlExporter.Default);
            AddExporter(CsvExporter.Default);

            // Agregar proveedores de columnas para que la tabla de resultados no esté vacía
            AddColumnProvider(DefaultColumnProviders.Instance);
        }
    }
}