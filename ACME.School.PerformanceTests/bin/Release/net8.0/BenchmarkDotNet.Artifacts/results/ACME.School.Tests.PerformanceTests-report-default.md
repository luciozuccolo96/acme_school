
BenchmarkDotNet v0.13.12, Windows 10 (10.0.19045.4412/22H2/2022Update)
11th Gen Intel Core i5-1135G7 2.40GHz, 1 CPU, 8 logical and 4 physical cores
.NET SDK 8.0.100
  [Host]     : .NET 8.0.0 (8.0.23.53103), X64 RyuJIT AVX-512F+CD+BW+DQ+VL+VBMI
  Job-HHGVML : .NET 8.0.0 (8.0.23.53103), X64 RyuJIT AVX-512F+CD+BW+DQ+VL+VBMI

IterationCount=3  WarmupCount=1  

 Method                         | Mean         | Error          | StdDev      |
------------------------------- |-------------:|---------------:|------------:|
 EnrollStudentsInCourses        | 7,716.249 μs | 17,182.8293 μs | 941.8489 μs |
 ListEnrollmentsWithinDateRange |     2.549 μs |      0.6203 μs |   0.0340 μs |
