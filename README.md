# Sistema de Gestión Escolar de ACME

## Descripción

Este proyecto es una Prueba de Concepto (PoC) para la gestión de cursos y estudiantes en la escuela ACME. El sistema permite:

- Registrar estudiantes especificando su nombre y edad (solo adultos pueden registrarse).
- Registrar cursos con un nombre, tarifa de inscripción, fecha de inicio y fecha de finalización.
- Permitir que un estudiante se inscriba en un curso, después de pagar la tarifa de inscripción si corresponde, a través de un gateway de pago.
- Listar los estudiantes y sus cursos dentro de un rango de fechas.

Está diseñada para demostrar la funcionalidad básica del sistema sin utilizar bases de datos ni APIs externas. La ejecución de las pruebas unitarias será suficiente para demostrar la corrección del programa. Este enfoque permite centrarse en las buenas prácticas y en la preparación del código para una futura escalabilidad y reutilización.

## Cómo Bajar el Proyecto y Ejecutar los Tests

1. Clona el repositorio:
```
git clone https://gitlab.com/luciozuccolo96/acme_school.git
```
2. Navega al directorio del proyecto:
```
cd acme_school
```
3. Restaura los paquetes NuGet:
```
dotnet restore
```
4. Ejecuta las pruebas unitarias:
```
dotnet test ACME.School.Tests
```
5. (Opcional) Ejecuta las pruebas de rendimiento:
```
dotnet run -c Release -p ACME.School.PerformanceTests
```

## Desarrollo y Estrategias Utilizadas

### Arquitectura Basada en Capas

El proyecto sigue una arquitectura en capas que separa las responsabilidades en diferentes proyectos:

- **ACME.School.Core**: Contiene los modelos de dominio (`Course`, `Enrollment`, `Student`) y los servicios (`IPaymentGateway`).
- **ACME.School.Tests**: Contiene las pruebas unitarias utilizando `xUnit`, `FluentAssertions` y `NSubstitute`.
- **ACME.School.PerformanceTests**: Contiene las pruebas de rendimiento utilizando `BenchmarkDotNet`.

### Escalabilidad

1. **Separación de Responsabilidades**: La arquitectura en capas asegura que los componentes del sistema sean modulares y fácilmente mantenibles.
2. **Abstracciones y Interfaces**: El uso de interfaces como `IPaymentGateway` permite sustituir fácilmente las implementaciones concretas, facilitando la integración con futuros servicios externos.
3. **Pruebas Rigurosas**: Las pruebas unitarias y de rendimiento aseguran que el sistema funcione correctamente y sea capaz de manejar grandes volúmenes de datos.

### Pruebas de Performance

Se han añadido pruebas de rendimiento utilizando `BenchmarkDotNet` para asegurar que el sistema pueda manejar grandes cantidades de solicitudes. Esto crea una base sólida para evaluar y mejorar el rendimiento desde el principio.

### Utilización de FluentAssertions y NSubstitute

- **FluentAssertions**: Facilita la escritura de aserciones en las pruebas de una manera más legible y expresiva.
- **NSubstitute**: Permite la creación de mocks y stubs para las pruebas, facilitando la simulación de comportamientos de dependencias externas.

### Posibles Nuevas Funcionalidades

El sistema, desarrollado en .NET, puede ser extendido con una variedad de funcionalidades adicionales:

- **Identidad de Estudiantes y Cursos**: Integración con sistemas de identidad para gestionar autenticación y autorización.
- **Profesores y Materias**: Registro y gestión de profesores, asignación de materias y seguimiento de calificaciones.
- **Certificados**: Emisión de certificados para estudiantes que completen cursos.
- **Funcionalidades Adicionales**: Agregar notificaciones, reportes, y dashboards para una mejor visualización y gestión de datos.

## Futura Utilización de Tecnologías y Escalabilidad

### Base de Datos utilizando Entity Framework

- **Persistencia de Datos**: Entity Framework (EF) permite la persistencia de datos de manera sencilla y eficiente, utilizando LINQ para interactuar con la base de datos.
- **Consultas Eficientes**: EF facilita la creación de consultas complejas y optimizadas, permitiendo el uso de LINQ para acceder y manipular datos.
- **Migraciones**: EF soporta migraciones, lo que permite mantener la base de datos sincronizada con el modelo de datos de la aplicación.
- **Integridad de Datos**: EF asegura la consistencia y validez de los datos mediante el uso de validaciones y restricciones en el modelo de datos.

### APIs y Controladores

- **Controladores RESTful**: Implementar APIs RESTful utilizando ASP.NET Core para exponer los servicios de la aplicación. Los controladores pueden seguir los principios SOLID para asegurar que sean mantenibles y escalables.
- **Inyección de Dependencias**: Utilizar el sistema de inyección de dependencias de ASP.NET Core para gestionar la creación y el ciclo de vida de los servicios y repositorios.
- **DTOs y Mapeo**: Utilizar Data Transfer Objects (DTOs) para transferir datos entre el cliente y el servidor, y AutoMapper para mapear entre los modelos de dominio y los DTOs.
- **Manejo de Errores**: Implementar un middleware de manejo de errores para capturar y gestionar excepciones de manera centralizada.

### Escalabilidad y Buenas Prácticas

- **Inyección de Dependencias**: Facilita la gestión de dependencias y la configuración de servicios, asegurando que los componentes sean fácilmente sustituibles y testeables.
- **Clases Abstractas e Interfaces**: Utilizar clases abstractas e interfaces para definir contratos y comportamientos comunes. Por ejemplo, tanto `Alumno` como `Profesor` pueden heredar de una clase abstracta `Persona`, pero implementar interfaces diferentes según sus roles específicos.
- **Servicios y Repositorios**: Implementar patrones de servicios y repositorios para encapsular la lógica de negocio y el acceso a datos, respetando el principio de responsabilidad única.
- **Polimorfismo y Herencia**: Utilizar polimorfismo y herencia para manejar diferentes roles y comportamientos en la aplicación. Por ejemplo, `Alumno` y `Profesor` pueden compartir propiedades comunes pero tener métodos específicos según su rol.
- **Test Driven Development (TDD)**: Adoptar TDD para asegurar que cada componente sea testeado adecuadamente antes de ser implementado, mejorando la calidad y la mantenibilidad del código.
- **Autenticación y Autorización**: Implementar sistemas de autenticación y autorización para gestionar el acceso a los recursos de la aplicación, utilizando ASP.NET Core Identity y políticas de autorización.

Estas prácticas y tecnologías permiten desarrollar una aplicación robusta y escalable, asegurando que esté preparada para crecer y adaptarse a futuras necesidades y demandas.

## Tiempo Invertido

En total, se han invertido aproximadamente 7-8 horas en el desarrollo de esta PoC, distribuidas de la siguiente manera:

- Realicé una breve investigación para determinar la mejor manera de plantear el desarrollo, revisando tanto recursos en internet como proyectos en los que ya había trabajado para lograr el mejor enfoque: 1 hora.
- El desarrollo del proyecto tomó entre 4 y 5 horas, siendo breve ya que es una PoC.
- Finalmente, dediqué 2 horas para explicar de manera detallada en este README cómo fue el desarrollo y qué metodologías se pueden utilizar para llevar a cabo el proyecto, ya que ante la simpleza de la PoC puede no apreciarse el alcance completo.