﻿using System;
using System.Collections.Generic;
using ACME.School.Core.Models;
using ACME.School.Core.Services;
using NSubstitute;

namespace ACME.School.Tests
{
    public class TestBase
    {
        protected List<Student> Students { get; private set; }
        protected List<Course> Courses { get; private set; }
        protected IPaymentGateway PaymentGateway { get; private set; }

        public TestBase()
        {
            InitializeData();
        }

        private void InitializeData()
        {
            Students = new List<Student>
            {
                new Student("Ron Weasley", 24),
                new Student("Harry Potter", 20),
                new Student("Hermione Granger", 22)
            };

            var now = DateTime.Now;
            Courses = new List<Course>
            {
                new Course("Defence Against the Dark Arts", 100, now, now.AddMonths(1)),
                new Course("Astronomy", 150, now.AddDays(1), now.AddMonths(2)),
                new Course("Potions", 200, now.AddDays(2), now.AddMonths(1)),
                new Course("Muggle Studies", 120, now.AddDays(3), now.AddMonths(3))
            };

            PaymentGateway = Substitute.For<IPaymentGateway>();
        }
    }
}
