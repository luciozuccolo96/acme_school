﻿using System;
using System.Linq;
using ACME.School.Core.Models;
using ACME.School.Core.Services;
using FluentAssertions;
using NSubstitute;
using Xunit;

namespace ACME.School.Tests
{
    public class StudentTests : TestBase
    {
        [Fact]
        public void CannotRegisterStudentWithInvalidName()
        {
            // Verifica que no se puedan registrar estudiantes con nombres inválidos.
            Action act1 = () => new Student("", 20);
            Action act2 = () => new Student(null, 20);

            act1.Should().Throw<ArgumentException>().WithMessage("Student name cannot be null or empty.");
            act2.Should().Throw<ArgumentException>().WithMessage("Student name cannot be null or empty.");
        }

        [Fact]
        public void CannotRegisterUnderageStudent()
        {
            // Verifica que no se pueda registrar un estudiante menor de edad.
            Action act = () => new Student("Albus Severus Potter", 12);
            act.Should().Throw<ArgumentException>().WithMessage("Only adults can register.");
        }

        [Fact]
        public void CanRegisterAdultStudent()
        {
            // Verifica que un estudiante adulto se registre correctamente.
            var student = Students.First(s => s.Name == "Ron Weasley");
            student.Name.Should().Be("Ron Weasley");
            student.Age.Should().Be(24);
        }

        [Fact]
        public void StudentCanEnrollInCourse()
        {
            // Verifica que un estudiante pueda inscribirse en un curso.
            var student = Students.First(s => s.Name == "Harry Potter");
            var course = Courses.First(c => c.Name == "Defence Against the Dark Arts");

            student.EnrollInCourse(course, PaymentGateway);

            student.Enrollments.Should().ContainSingle(); // Verifica que hay una inscripción.
            PaymentGateway.Received(1).ProcessPayment(course.RegistrationFee); // Verifica que se realizó el pago.
        }

        [Fact]
        public void CourseFeeIsChargedCorrectly()
        {
            // Verifica que se cobra correctamente la matrícula del curso.
            var student = Students.First(s => s.Name == "Hermione Granger");
            var course = Courses.First(c => c.Name == "Muggle Studies");

            student.EnrollInCourse(course, PaymentGateway);

            PaymentGateway.Received(1).ProcessPayment(course.RegistrationFee); // Verifica que se procesó el pago correctamente.
        }

        [Fact]
        public void CannotRegisterCourseWithInvalidDates()
        {
            // Verifica que no se puedan registrar cursos con fechas inválidas.
            Action act = () => new Course("Divination", 100, DateTime.Now.AddMonths(1), DateTime.Now);
            act.Should().Throw<ArgumentException>().WithMessage("The start date cannot be later than the end date.");
        }

        [Fact]
        public void CannotEnrollInCourseWithoutPayment()
        {
            // Verifica que no se pueda inscribir en un curso sin realizar el pago.
            var student = Students.First(s => s.Name == "Ron Weasley");
            var course = Courses.First(c => c.Name == "Astronomy");
            PaymentGateway.When(pg => pg.ProcessPayment(Arg.Any<decimal>())).Do(x => { throw new Exception("Payment failed"); });

            Action act = () => student.EnrollInCourse(course, PaymentGateway);
            act.Should().Throw<Exception>().WithMessage("Payment failed"); // Verifica que se lanza una excepción si el pago falla.
            student.Enrollments.Should().BeEmpty(); // Verifica que no hay inscripciones.
        }

        [Fact]
        public void ListEnrollmentsWithinDateRange()
        {
            // Verifica que solo las inscripciones dentro del rango de fechas se devuelvan.
            var fixedDate = new DateTime(2024, 5, 28);
            var existingStudent = Students.First(s => s.Name == "Hermione Granger");
            var existingCourse1 = Courses.First(c => c.Name == "Defence Against the Dark Arts");
            var existingCourse2 = Courses.First(c => c.Name == "Potions");

            var enrollment1 = new Enrollment(existingStudent, existingCourse1, fixedDate.AddDays(5)); // Fecha dentro del rango
            var enrollment2 = new Enrollment(existingStudent, existingCourse2, fixedDate.AddMonths(2)); // Fecha fuera del rango

            existingStudent.Enrollments.Add(enrollment1);
            existingStudent.Enrollments.Add(enrollment2);

            var startDate = fixedDate;
            var endDate = fixedDate.AddMonths(1);
            var enrollments = existingStudent.Enrollments.Where(e => e.EnrollmentDate >= startDate && e.EnrollmentDate <= endDate).ToList();

            enrollments.Should().HaveCount(1); // Verifica que solo hay una inscripción dentro del rango.
            enrollments[0].Course.Name.Should().Be("Defence Against the Dark Arts"); // Verifica que es el curso correcto.
            enrollments[0].EnrollmentDate.Should().Be(fixedDate.AddDays(5)); // Verifica que la fecha de inscripción es correcta.
        }

        [Fact]
        public void StudentEnrollmentsAreUpdatedCorrectly()
        {
            // Verifica que las inscripciones de un estudiante se actualizan correctamente.
            var student = Students.First(s => s.Name == "Harry Potter");
            var course1 = Courses.First(c => c.Name == "Defence Against the Dark Arts");
            var course2 = Courses.First(c => c.Name == "Astronomy");

            student.EnrollInCourse(course1, PaymentGateway);
            student.EnrollInCourse(course2, PaymentGateway);

            student.Enrollments.Should().HaveCount(2); // Verifica que hay dos inscripciones.
        }
    }
}

